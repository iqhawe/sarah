


$( document ).ready(function() {

$("head").append(
`<style>
  /* icon imported */

  i.xs svg {
    height: 15px;
    width: 15px;
  }
  i.s svg {
    height: 20px;
    width: 20px;
  }
  i.m svg {
    height: 25px;
    width: 25px;
  }
  i.l svg {
    height: 30px;
    width: 30px;
  }
  i.xl svg {
    height: 35px;
    width: 35px;
  }
  i.xxl svg {
    height: 40px;
    width: 40px;
  }
</style>`);
IconAct()

})


function IconAct() {
  let d;
  $('i').html(function() {
    if($(this).attr("oncolor")) {
      $(this).mouseenter(function() {
        $(this).css("color", $(this).attr("oncolor"));
      })
    }

    if($(this).attr("color")) {
      $(this).css("color", $(this).attr("color"));
      $(this).mouseleave(function() {
        $(this).css("color", $(this).attr("color"));
      })
    }

    switch ($(this).attr("icon")) {
      case "microphone":
        return (`<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path fill="currentColor" d="M160 352c53.02 0 96-42.98 96-96V96c0-53.02-42.98-96-96-96S64 42.98 64 96v160c0 53.02 42.98 96 96 96zM96 96c0-35.29 28.71-64 64-64s64 28.71 64 64v160c0 35.29-28.71 64-64 64s-64-28.71-64-64V96zm216 96h-16c-4.42 0-8 3.58-8 8v56c0 73.46-62.2 132.68-136.73 127.71C83.3 379.18 32 319.61 32 251.49V200c0-4.42-3.58-8-8-8H8c-4.42 0-8 3.58-8 8v50.34c0 83.39 61.65 156.12 144 164.43V480H72c-4.42 0-8 3.58-8 8v16c0 4.42 3.58 8 8 8h176c4.42 0 8-3.58 8-8v-16c0-4.42-3.58-8-8-8h-72v-65.01C256.71 406.9 320 338.8 320 256v-56c0-4.42-3.58-8-8-8z" class=""></path></svg>`);
        break;
      case "microphone-slash":
        return(`<svg aria-hidden="true" focusable="false" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M256 96c0-35.3 28.7-64 64-64s64 28.7 64 64v149.3l30.7 24.1c.6-4.4 1.3-8.8 1.3-13.4V96c0-53-43-96-96-96s-96 43-96 96v23.3l32 25.2zm224 160v-56c0-4.4-3.6-8-8-8h-16c-4.4 0-8 3.6-8 8v56c0 12.3-2.3 24-5.6 35.3l27 21.3C476.2 295 480 276 480 256zm-72 224h-72v-65c27.2-2.7 52.1-12.7 73.6-27.3l-26.4-20.8c-21 12-45.6 18.6-71.9 16.8-68-4.5-119.3-64.1-119.3-132.2v-35.2l-28.5-22.5c-2 1.5-3.5 3.5-3.5 6.1v50.3c0 83.4 61.7 156.1 144 164.4V480h-72c-4.4 0-8 3.6-8 8v16c0 4.4 3.6 8 8 8h176c4.4 0 8-3.6 8-8v-16c0-4.4-3.6-8-8-8zm-88-128c12.6 0 24.5-2.6 35.5-7l-32.2-25.4c-1.1.1-2.1.3-3.3.3-31.3 0-57.3-22.6-62.8-52.4l-33.2-26V256c0 53 43 96 96 96zm317 133.2L23 1.8C19.6-1 14.5-.5 11.8 3l-10 12.5C-1 19-.4 24 3 26.7l614 483.5c3.4 2.8 8.5 2.2 11.2-1.2l10-12.5c2.8-3.5 2.3-8.5-1.2-11.3z" class=""></path></svg>`);
        break;
    }
  })
}
