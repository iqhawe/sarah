const YouTube = require('simple-youtube-api');
const ytb = new YouTube(key='AIzaSyAmm0tAApu3XivE_1n7xbqSkZxs2Ql59HU');

const e = "sarah";
const commands= [
  `joue`,
  `ajoute à la playlist`,
  `stop`,
  `pause`,
  `suivant`,
  `monte le son`,
  `baisse le son`,
  `mets le son à`,
  `continue`,
  `arrête le PC`,
  `aide-moi`
]
const alias = [
  `ajoute à la file d'attente`,
  `ajoute`,
  `pose`,
  `quelles sont les commandes`,
  `arrête tout`,
  `arrête la musique`,
  `baisse le volume`,
  `baisse le volume`,
  `mets le volume à`
]


// ########################### CLASS ET FONCTION #############################

function play(args) {
  ytb.searchVideos(args, 1)
      .then(results => {

          console.log("Url : "+results[0].url);

          let id = (results[0].url).replace("https://www.youtube.com/watch?v=", '');
          player.loadVideoById(id);
          /*
          let el = document.getElementById('player');
          el.src=`https://www.youtube.com/embed/${id}?disablekb=1&fs=0&iv_load_policy=3&modestbranding=1&rel=0&enablejsapi=1&origin=http%3A%2F%2F${e.toLowerCase()}.localhost&widgetid=1&autoplay=1`;
          */
        })
      .catch(console.log);
}

function forward() {
  if (!ytb_queue[0]) {
    if(player.getPlayerState() == 1) {
      player.stopVideo();
    }
    Chat.out("La playlist est finie");
    return
  } else {
    Chat.type(`Musique suivante (${ytb_queue[0]})`);
    ytb.searchVideos(ytb_queue[0], 1)
        .then(results => {
            ytb_queue.shift();
            console.log("Url : "+results[0].url);

            let id = (results[0].url).replace("https://www.youtube.com/watch?v=", '');
            player.loadVideoById(id);
          })
          .catch(console.log);
    }
}



class chat {
  constructor(el) {
  }
  //Message recu :
  in(message) {


    //Mappage du message:
    //Master world (e):
    var masterworld = false;
    if(message.includes(e)) {
      masterworld= true;
    }
    console.log(`masterworld: ${masterworld}`);
    //Commande
    var commande = message.replace(e, '');
    for (let i = 0; i < commands.length; i++) {
      if(commande.includes(commands[i])) {
        commande =commands[i];
        console.log(`Cmd: ${commande}`);
      }
    }
    //alias
    for (let i = 0; i < alias.length; i++) {
      if(commande.includes(alias[i])) {
        commande =alias[i];
        console.log(`Cmd: ${commande}`);
      }
    }
    //Arguments
    var args = (message.replace(e,'')).replace(commande, '');
    console.log(`Args: ${args}`);



    let el = document.getElementById('console');
    el.innerHTML+=`<div class="line in">${message}</div>`;



    // Action en fonction du message :
    this.act(message, commande, args);
  }

  act(message, commande, args) {
    var actualVolume = player.getVolume();
    switch(commande) {
    case "aide-moi":
    case "quelles sont les commandes":
      this.out("Voici toutes les commandes :");
      this.type(commands);
      break;
    case "joue":
      this.out(`D'accord, je joue ${args}`);
      play(args);
      break;
    case "ajoute à la playlist":
    case "ajoute à la file d'attente":
    case "ajoute":
      this.out(`J'ai ajouté ${args} à la playlist`);
      ytb_queue.push(args);
      break;
    case "stop":
    case "arrête tout":
    case "arrête la musique":
      player.stopVideo();
      this.out("J'arrête la musique");
      break;
    case "pause":
    case "pose":
      this.type("Vidéo en pause");
      player.pauseVideo();
      break;
    case "continue":
      this.type("C'est repartit!");
      player.playVideo();
      break;
    case "suivant":
      forward();
      break;
    case "baisse le son":
    case "baisse le volume":
      if (actualVolume==0) {
        this.out('Le son est déjà au minimum');
      } else {
        this.type(`Le son est désormais à ${actualVolume}`);
        player.setVolume(actualVolume - 10);
      }
      break;
    case "monte le son":
    case "monte le volume":
      if (actualVolume==100) {
        this.out('Le son est déjà au maximum');
      } else {
        this.type(`Le son est désormais à ${actualVolume}`);
        player.setVolume(actualVolume + 10);
      }
      break;
    case "mets le son à":
    case "mets le volume à":
      if (args > 100) {
        this.type('Volume mis à 100');
        player.setVolume(100);
      } else if (args < 0) {
        this.type('Volume mis à 0');
        player.setVolume(0);
      } else if (args == "zéro") {
        player.setVolume(0);
        this.type(`C'est fait!`);
      } else {
        player.setVolume(args);
        this.type(`C'est fait!`);
      }
      break;
    case "arrête le PC":
      document.location.href ="/shutdown.php";
      break;
    default:
      this.type("Pas compris");
      break;
    // code block
    demarrerReconnaissanceVocale();
  }
}


  out(message) {
    // Synthèse vocale de ce qui a été reconnu
    var u = new SpeechSynthesisUtterance();
    u.text = message;
    u.lang = 'fr-FR';
    u.rate = 1.2;
    speechSynthesis.speak(u);
    let el = document.getElementById('console');
    el.innerHTML+=`<div class="line out">${message}</div>`;
  }
  type(message) {
    let el = document.getElementById('console');
    el.innerHTML+=`<div class="line out">${message}</div>`;
  }
  say(message) {
    // Synthèse vocale de ce qui a été reconnu
    var u = new SpeechSynthesisUtterance();
    u.text = message;
    u.lang = 'fr-FR';
    u.rate = 1.2;
    speechSynthesis.speak(u);
  }

}

//##############################################################################
// Initialisation de la reconnaissance vocale en fonction du navigateur
// Pour l'instant, seul Google Chrome le supporte
var SpeechRecognition = SpeechRecognition ||
                          webkitSpeechRecognition ||
                          mozSpeechRecognition ||
                          msSpeechRecognition ||
                          oSpeechRecognition;

var recognition;
var lastStartedAt;
var Chat = new chat('console');

function listen() {
if (!SpeechRecognition) {
    console.log('Pas de reconnaissance vocale disponible');
    alert('Pas de reconnaissance vocale disponible');
} else {

    // Arrêt de l'ensemble des instances déjà démarrées
        if (recognition && recognition.abort) {
        recognition.abort();
        }

    // Initialisation de la reconnaissance vocale
    recognition = new SpeechRecognition();
    // Reconnaissance en continue
    recognition.continuous = true;
    // Langue française
    recognition.lang = 'fr-FR';

    // Evènement de début de la reconnaissance vocale
    recognition.onstart = function() {
        console.log('Démarrage de la reconnaissance');
    };

    // Evènement de fin de la reconnaissance vocale
    // A la fin de la reconnaissance (timeout), il est nécessaire de la redémarrer pour avoir une reconnaissance en continu
    recognition.onend = function() {
        console.log('Fin de la reconnaissance');
        var timeSinceLastStart = new Date().getTime()-lastStartedAt;
        if (timeSinceLastStart < 1000) {
            setTimeout(demarrerReconnaissanceVocale, 1000-timeSinceLastStart);
        } else {
            // Démarrage de la reconnaissance vocale
            demarrerReconnaissanceVocale();
        }
    };

    // Evènement de résultat de la reconnaissance vocale
    recognition.onresult = function (event) {
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            var texteReconnu = event.results[i][0].transcript;
            console.log("----------------------");
            console.log('Résultat = ' + texteReconnu);
            //Analyse du message
            Chat.in(texteReconnu.toLowerCase());

        }
    };

    // Démarrage de la reconnaissance vocale
    demarrerReconnaissanceVocale();
}
}


function demarrerReconnaissanceVocale() {
// Démarrage de la reconnaissance vocale
console.log("Listen..");
lastStartedAt = new Date().getTime();
    recognition.start();
}

window.onload = listen();
