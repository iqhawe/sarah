(function(){function r(e,n,t){function o(i,f){if(!n[i]){if(!e[i]){var c="function"==typeof require&&require;if(!f&&c)return c(i,!0);if(u)return u(i,!0);var a=new Error("Cannot find module '"+i+"'");throw a.code="MODULE_NOT_FOUND",a}var p=n[i]={exports:{}};e[i][0].call(p.exports,function(r){var n=e[i][1][r];return o(n||r)},p,p.exports,r,e,n,t)}return n[i].exports}for(var u="function"==typeof require&&require,i=0;i<t.length;i++)o(t[i]);return o}return r})()({1:[function(require,module,exports){
(function (global){
/*! https://mths.be/punycode v1.4.1 by @mathias */
;(function(root) {

	/** Detect free variables */
	var freeExports = typeof exports == 'object' && exports &&
		!exports.nodeType && exports;
	var freeModule = typeof module == 'object' && module &&
		!module.nodeType && module;
	var freeGlobal = typeof global == 'object' && global;
	if (
		freeGlobal.global === freeGlobal ||
		freeGlobal.window === freeGlobal ||
		freeGlobal.self === freeGlobal
	) {
		root = freeGlobal;
	}

	/**
	 * The `punycode` object.
	 * @name punycode
	 * @type Object
	 */
	var punycode,

	/** Highest positive signed 32-bit float value */
	maxInt = 2147483647, // aka. 0x7FFFFFFF or 2^31-1

	/** Bootstring parameters */
	base = 36,
	tMin = 1,
	tMax = 26,
	skew = 38,
	damp = 700,
	initialBias = 72,
	initialN = 128, // 0x80
	delimiter = '-', // '\x2D'

	/** Regular expressions */
	regexPunycode = /^xn--/,
	regexNonASCII = /[^\x20-\x7E]/, // unprintable ASCII chars + non-ASCII chars
	regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g, // RFC 3490 separators

	/** Error messages */
	errors = {
		'overflow': 'Overflow: input needs wider integers to process',
		'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
		'invalid-input': 'Invalid input'
	},

	/** Convenience shortcuts */
	baseMinusTMin = base - tMin,
	floor = Math.floor,
	stringFromCharCode = String.fromCharCode,

	/** Temporary variable */
	key;

	/*--------------------------------------------------------------------------*/

	/**
	 * A generic error utility function.
	 * @private
	 * @param {String} type The error type.
	 * @returns {Error} Throws a `RangeError` with the applicable error message.
	 */
	function error(type) {
		throw new RangeError(errors[type]);
	}

	/**
	 * A generic `Array#map` utility function.
	 * @private
	 * @param {Array} array The array to iterate over.
	 * @param {Function} callback The function that gets called for every array
	 * item.
	 * @returns {Array} A new array of values returned by the callback function.
	 */
	function map(array, fn) {
		var length = array.length;
		var result = [];
		while (length--) {
			result[length] = fn(array[length]);
		}
		return result;
	}

	/**
	 * A simple `Array#map`-like wrapper to work with domain name strings or email
	 * addresses.
	 * @private
	 * @param {String} domain The domain name or email address.
	 * @param {Function} callback The function that gets called for every
	 * character.
	 * @returns {Array} A new string of characters returned by the callback
	 * function.
	 */
	function mapDomain(string, fn) {
		var parts = string.split('@');
		var result = '';
		if (parts.length > 1) {
			// In email addresses, only the domain name should be punycoded. Leave
			// the local part (i.e. everything up to `@`) intact.
			result = parts[0] + '@';
			string = parts[1];
		}
		// Avoid `split(regex)` for IE8 compatibility. See #17.
		string = string.replace(regexSeparators, '\x2E');
		var labels = string.split('.');
		var encoded = map(labels, fn).join('.');
		return result + encoded;
	}

	/**
	 * Creates an array containing the numeric code points of each Unicode
	 * character in the string. While JavaScript uses UCS-2 internally,
	 * this function will convert a pair of surrogate halves (each of which
	 * UCS-2 exposes as separate characters) into a single code point,
	 * matching UTF-16.
	 * @see `punycode.ucs2.encode`
	 * @see <https://mathiasbynens.be/notes/javascript-encoding>
	 * @memberOf punycode.ucs2
	 * @name decode
	 * @param {String} string The Unicode input string (UCS-2).
	 * @returns {Array} The new array of code points.
	 */
	function ucs2decode(string) {
		var output = [],
		    counter = 0,
		    length = string.length,
		    value,
		    extra;
		while (counter < length) {
			value = string.charCodeAt(counter++);
			if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
				// high surrogate, and there is a next character
				extra = string.charCodeAt(counter++);
				if ((extra & 0xFC00) == 0xDC00) { // low surrogate
					output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
				} else {
					// unmatched surrogate; only append this code unit, in case the next
					// code unit is the high surrogate of a surrogate pair
					output.push(value);
					counter--;
				}
			} else {
				output.push(value);
			}
		}
		return output;
	}

	/**
	 * Creates a string based on an array of numeric code points.
	 * @see `punycode.ucs2.decode`
	 * @memberOf punycode.ucs2
	 * @name encode
	 * @param {Array} codePoints The array of numeric code points.
	 * @returns {String} The new Unicode string (UCS-2).
	 */
	function ucs2encode(array) {
		return map(array, function(value) {
			var output = '';
			if (value > 0xFFFF) {
				value -= 0x10000;
				output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
				value = 0xDC00 | value & 0x3FF;
			}
			output += stringFromCharCode(value);
			return output;
		}).join('');
	}

	/**
	 * Converts a basic code point into a digit/integer.
	 * @see `digitToBasic()`
	 * @private
	 * @param {Number} codePoint The basic numeric code point value.
	 * @returns {Number} The numeric value of a basic code point (for use in
	 * representing integers) in the range `0` to `base - 1`, or `base` if
	 * the code point does not represent a value.
	 */
	function basicToDigit(codePoint) {
		if (codePoint - 48 < 10) {
			return codePoint - 22;
		}
		if (codePoint - 65 < 26) {
			return codePoint - 65;
		}
		if (codePoint - 97 < 26) {
			return codePoint - 97;
		}
		return base;
	}

	/**
	 * Converts a digit/integer into a basic code point.
	 * @see `basicToDigit()`
	 * @private
	 * @param {Number} digit The numeric value of a basic code point.
	 * @returns {Number} The basic code point whose value (when used for
	 * representing integers) is `digit`, which needs to be in the range
	 * `0` to `base - 1`. If `flag` is non-zero, the uppercase form is
	 * used; else, the lowercase form is used. The behavior is undefined
	 * if `flag` is non-zero and `digit` has no uppercase form.
	 */
	function digitToBasic(digit, flag) {
		//  0..25 map to ASCII a..z or A..Z
		// 26..35 map to ASCII 0..9
		return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
	}

	/**
	 * Bias adaptation function as per section 3.4 of RFC 3492.
	 * https://tools.ietf.org/html/rfc3492#section-3.4
	 * @private
	 */
	function adapt(delta, numPoints, firstTime) {
		var k = 0;
		delta = firstTime ? floor(delta / damp) : delta >> 1;
		delta += floor(delta / numPoints);
		for (/* no initialization */; delta > baseMinusTMin * tMax >> 1; k += base) {
			delta = floor(delta / baseMinusTMin);
		}
		return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
	}

	/**
	 * Converts a Punycode string of ASCII-only symbols to a string of Unicode
	 * symbols.
	 * @memberOf punycode
	 * @param {String} input The Punycode string of ASCII-only symbols.
	 * @returns {String} The resulting string of Unicode symbols.
	 */
	function decode(input) {
		// Don't use UCS-2
		var output = [],
		    inputLength = input.length,
		    out,
		    i = 0,
		    n = initialN,
		    bias = initialBias,
		    basic,
		    j,
		    index,
		    oldi,
		    w,
		    k,
		    digit,
		    t,
		    /** Cached calculation results */
		    baseMinusT;

		// Handle the basic code points: let `basic` be the number of input code
		// points before the last delimiter, or `0` if there is none, then copy
		// the first basic code points to the output.

		basic = input.lastIndexOf(delimiter);
		if (basic < 0) {
			basic = 0;
		}

		for (j = 0; j < basic; ++j) {
			// if it's not a basic code point
			if (input.charCodeAt(j) >= 0x80) {
				error('not-basic');
			}
			output.push(input.charCodeAt(j));
		}

		// Main decoding loop: start just after the last delimiter if any basic code
		// points were copied; start at the beginning otherwise.

		for (index = basic > 0 ? basic + 1 : 0; index < inputLength; /* no final expression */) {

			// `index` is the index of the next character to be consumed.
			// Decode a generalized variable-length integer into `delta`,
			// which gets added to `i`. The overflow checking is easier
			// if we increase `i` as we go, then subtract off its starting
			// value at the end to obtain `delta`.
			for (oldi = i, w = 1, k = base; /* no condition */; k += base) {

				if (index >= inputLength) {
					error('invalid-input');
				}

				digit = basicToDigit(input.charCodeAt(index++));

				if (digit >= base || digit > floor((maxInt - i) / w)) {
					error('overflow');
				}

				i += digit * w;
				t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);

				if (digit < t) {
					break;
				}

				baseMinusT = base - t;
				if (w > floor(maxInt / baseMinusT)) {
					error('overflow');
				}

				w *= baseMinusT;

			}

			out = output.length + 1;
			bias = adapt(i - oldi, out, oldi == 0);

			// `i` was supposed to wrap around from `out` to `0`,
			// incrementing `n` each time, so we'll fix that now:
			if (floor(i / out) > maxInt - n) {
				error('overflow');
			}

			n += floor(i / out);
			i %= out;

			// Insert `n` at position `i` of the output
			output.splice(i++, 0, n);

		}

		return ucs2encode(output);
	}

	/**
	 * Converts a string of Unicode symbols (e.g. a domain name label) to a
	 * Punycode string of ASCII-only symbols.
	 * @memberOf punycode
	 * @param {String} input The string of Unicode symbols.
	 * @returns {String} The resulting Punycode string of ASCII-only symbols.
	 */
	function encode(input) {
		var n,
		    delta,
		    handledCPCount,
		    basicLength,
		    bias,
		    j,
		    m,
		    q,
		    k,
		    t,
		    currentValue,
		    output = [],
		    /** `inputLength` will hold the number of code points in `input`. */
		    inputLength,
		    /** Cached calculation results */
		    handledCPCountPlusOne,
		    baseMinusT,
		    qMinusT;

		// Convert the input in UCS-2 to Unicode
		input = ucs2decode(input);

		// Cache the length
		inputLength = input.length;

		// Initialize the state
		n = initialN;
		delta = 0;
		bias = initialBias;

		// Handle the basic code points
		for (j = 0; j < inputLength; ++j) {
			currentValue = input[j];
			if (currentValue < 0x80) {
				output.push(stringFromCharCode(currentValue));
			}
		}

		handledCPCount = basicLength = output.length;

		// `handledCPCount` is the number of code points that have been handled;
		// `basicLength` is the number of basic code points.

		// Finish the basic string - if it is not empty - with a delimiter
		if (basicLength) {
			output.push(delimiter);
		}

		// Main encoding loop:
		while (handledCPCount < inputLength) {

			// All non-basic code points < n have been handled already. Find the next
			// larger one:
			for (m = maxInt, j = 0; j < inputLength; ++j) {
				currentValue = input[j];
				if (currentValue >= n && currentValue < m) {
					m = currentValue;
				}
			}

			// Increase `delta` enough to advance the decoder's <n,i> state to <m,0>,
			// but guard against overflow
			handledCPCountPlusOne = handledCPCount + 1;
			if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
				error('overflow');
			}

			delta += (m - n) * handledCPCountPlusOne;
			n = m;

			for (j = 0; j < inputLength; ++j) {
				currentValue = input[j];

				if (currentValue < n && ++delta > maxInt) {
					error('overflow');
				}

				if (currentValue == n) {
					// Represent delta as a generalized variable-length integer
					for (q = delta, k = base; /* no condition */; k += base) {
						t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);
						if (q < t) {
							break;
						}
						qMinusT = q - t;
						baseMinusT = base - t;
						output.push(
							stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0))
						);
						q = floor(qMinusT / baseMinusT);
					}

					output.push(stringFromCharCode(digitToBasic(q, 0)));
					bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
					delta = 0;
					++handledCPCount;
				}
			}

			++delta;
			++n;

		}
		return output.join('');
	}

	/**
	 * Converts a Punycode string representing a domain name or an email address
	 * to Unicode. Only the Punycoded parts of the input will be converted, i.e.
	 * it doesn't matter if you call it on a string that has already been
	 * converted to Unicode.
	 * @memberOf punycode
	 * @param {String} input The Punycoded domain name or email address to
	 * convert to Unicode.
	 * @returns {String} The Unicode representation of the given Punycode
	 * string.
	 */
	function toUnicode(input) {
		return mapDomain(input, function(string) {
			return regexPunycode.test(string)
				? decode(string.slice(4).toLowerCase())
				: string;
		});
	}

	/**
	 * Converts a Unicode string representing a domain name or an email address to
	 * Punycode. Only the non-ASCII parts of the domain name will be converted,
	 * i.e. it doesn't matter if you call it with a domain that's already in
	 * ASCII.
	 * @memberOf punycode
	 * @param {String} input The domain name or email address to convert, as a
	 * Unicode string.
	 * @returns {String} The Punycode representation of the given domain name or
	 * email address.
	 */
	function toASCII(input) {
		return mapDomain(input, function(string) {
			return regexNonASCII.test(string)
				? 'xn--' + encode(string)
				: string;
		});
	}

	/*--------------------------------------------------------------------------*/

	/** Define the public API */
	punycode = {
		/**
		 * A string representing the current Punycode.js version number.
		 * @memberOf punycode
		 * @type String
		 */
		'version': '1.4.1',
		/**
		 * An object of methods to convert from JavaScript's internal character
		 * representation (UCS-2) to Unicode code points, and back.
		 * @see <https://mathiasbynens.be/notes/javascript-encoding>
		 * @memberOf punycode
		 * @type Object
		 */
		'ucs2': {
			'decode': ucs2decode,
			'encode': ucs2encode
		},
		'decode': decode,
		'encode': encode,
		'toASCII': toASCII,
		'toUnicode': toUnicode
	};

	/** Expose `punycode` */
	// Some AMD build optimizers, like r.js, check for specific condition patterns
	// like the following:
	if (
		typeof define == 'function' &&
		typeof define.amd == 'object' &&
		define.amd
	) {
		define('punycode', function() {
			return punycode;
		});
	} else if (freeExports && freeModule) {
		if (module.exports == freeExports) {
			// in Node.js, io.js, or RingoJS v0.8.0+
			freeModule.exports = punycode;
		} else {
			// in Narwhal or RingoJS v0.7.0-
			for (key in punycode) {
				punycode.hasOwnProperty(key) && (freeExports[key] = punycode[key]);
			}
		}
	} else {
		// in Rhino or a web browser
		root.punycode = punycode;
	}

}(this));

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],2:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

'use strict';

// If obj.hasOwnProperty has been overridden, then calling
// obj.hasOwnProperty(prop) will break.
// See: https://github.com/joyent/node/issues/1707
function hasOwnProperty(obj, prop) {
  return Object.prototype.hasOwnProperty.call(obj, prop);
}

module.exports = function(qs, sep, eq, options) {
  sep = sep || '&';
  eq = eq || '=';
  var obj = {};

  if (typeof qs !== 'string' || qs.length === 0) {
    return obj;
  }

  var regexp = /\+/g;
  qs = qs.split(sep);

  var maxKeys = 1000;
  if (options && typeof options.maxKeys === 'number') {
    maxKeys = options.maxKeys;
  }

  var len = qs.length;
  // maxKeys <= 0 means that we should not limit keys count
  if (maxKeys > 0 && len > maxKeys) {
    len = maxKeys;
  }

  for (var i = 0; i < len; ++i) {
    var x = qs[i].replace(regexp, '%20'),
        idx = x.indexOf(eq),
        kstr, vstr, k, v;

    if (idx >= 0) {
      kstr = x.substr(0, idx);
      vstr = x.substr(idx + 1);
    } else {
      kstr = x;
      vstr = '';
    }

    k = decodeURIComponent(kstr);
    v = decodeURIComponent(vstr);

    if (!hasOwnProperty(obj, k)) {
      obj[k] = v;
    } else if (isArray(obj[k])) {
      obj[k].push(v);
    } else {
      obj[k] = [obj[k], v];
    }
  }

  return obj;
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};

},{}],3:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

'use strict';

var stringifyPrimitive = function(v) {
  switch (typeof v) {
    case 'string':
      return v;

    case 'boolean':
      return v ? 'true' : 'false';

    case 'number':
      return isFinite(v) ? v : '';

    default:
      return '';
  }
};

module.exports = function(obj, sep, eq, name) {
  sep = sep || '&';
  eq = eq || '=';
  if (obj === null) {
    obj = undefined;
  }

  if (typeof obj === 'object') {
    return map(objectKeys(obj), function(k) {
      var ks = encodeURIComponent(stringifyPrimitive(k)) + eq;
      if (isArray(obj[k])) {
        return map(obj[k], function(v) {
          return ks + encodeURIComponent(stringifyPrimitive(v));
        }).join(sep);
      } else {
        return ks + encodeURIComponent(stringifyPrimitive(obj[k]));
      }
    }).join(sep);

  }

  if (!name) return '';
  return encodeURIComponent(stringifyPrimitive(name)) + eq +
         encodeURIComponent(stringifyPrimitive(obj));
};

var isArray = Array.isArray || function (xs) {
  return Object.prototype.toString.call(xs) === '[object Array]';
};

function map (xs, f) {
  if (xs.map) return xs.map(f);
  var res = [];
  for (var i = 0; i < xs.length; i++) {
    res.push(f(xs[i], i));
  }
  return res;
}

var objectKeys = Object.keys || function (obj) {
  var res = [];
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) res.push(key);
  }
  return res;
};

},{}],4:[function(require,module,exports){
'use strict';

exports.decode = exports.parse = require('./decode');
exports.encode = exports.stringify = require('./encode');

},{"./decode":2,"./encode":3}],5:[function(require,module,exports){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

'use strict';

var punycode = require('punycode');
var util = require('./util');

exports.parse = urlParse;
exports.resolve = urlResolve;
exports.resolveObject = urlResolveObject;
exports.format = urlFormat;

exports.Url = Url;

function Url() {
  this.protocol = null;
  this.slashes = null;
  this.auth = null;
  this.host = null;
  this.port = null;
  this.hostname = null;
  this.hash = null;
  this.search = null;
  this.query = null;
  this.pathname = null;
  this.path = null;
  this.href = null;
}

// Reference: RFC 3986, RFC 1808, RFC 2396

// define these here so at least they only have to be
// compiled once on the first module load.
var protocolPattern = /^([a-z0-9.+-]+:)/i,
    portPattern = /:[0-9]*$/,

    // Special case for a simple path URL
    simplePathPattern = /^(\/\/?(?!\/)[^\?\s]*)(\?[^\s]*)?$/,

    // RFC 2396: characters reserved for delimiting URLs.
    // We actually just auto-escape these.
    delims = ['<', '>', '"', '`', ' ', '\r', '\n', '\t'],

    // RFC 2396: characters not allowed for various reasons.
    unwise = ['{', '}', '|', '\\', '^', '`'].concat(delims),

    // Allowed by RFCs, but cause of XSS attacks.  Always escape these.
    autoEscape = ['\''].concat(unwise),
    // Characters that are never ever allowed in a hostname.
    // Note that any invalid chars are also handled, but these
    // are the ones that are *expected* to be seen, so we fast-path
    // them.
    nonHostChars = ['%', '/', '?', ';', '#'].concat(autoEscape),
    hostEndingChars = ['/', '?', '#'],
    hostnameMaxLen = 255,
    hostnamePartPattern = /^[+a-z0-9A-Z_-]{0,63}$/,
    hostnamePartStart = /^([+a-z0-9A-Z_-]{0,63})(.*)$/,
    // protocols that can allow "unsafe" and "unwise" chars.
    unsafeProtocol = {
      'javascript': true,
      'javascript:': true
    },
    // protocols that never have a hostname.
    hostlessProtocol = {
      'javascript': true,
      'javascript:': true
    },
    // protocols that always contain a // bit.
    slashedProtocol = {
      'http': true,
      'https': true,
      'ftp': true,
      'gopher': true,
      'file': true,
      'http:': true,
      'https:': true,
      'ftp:': true,
      'gopher:': true,
      'file:': true
    },
    querystring = require('querystring');

function urlParse(url, parseQueryString, slashesDenoteHost) {
  if (url && util.isObject(url) && url instanceof Url) return url;

  var u = new Url;
  u.parse(url, parseQueryString, slashesDenoteHost);
  return u;
}

Url.prototype.parse = function(url, parseQueryString, slashesDenoteHost) {
  if (!util.isString(url)) {
    throw new TypeError("Parameter 'url' must be a string, not " + typeof url);
  }

  // Copy chrome, IE, opera backslash-handling behavior.
  // Back slashes before the query string get converted to forward slashes
  // See: https://code.google.com/p/chromium/issues/detail?id=25916
  var queryIndex = url.indexOf('?'),
      splitter =
          (queryIndex !== -1 && queryIndex < url.indexOf('#')) ? '?' : '#',
      uSplit = url.split(splitter),
      slashRegex = /\\/g;
  uSplit[0] = uSplit[0].replace(slashRegex, '/');
  url = uSplit.join(splitter);

  var rest = url;

  // trim before proceeding.
  // This is to support parse stuff like "  http://foo.com  \n"
  rest = rest.trim();

  if (!slashesDenoteHost && url.split('#').length === 1) {
    // Try fast path regexp
    var simplePath = simplePathPattern.exec(rest);
    if (simplePath) {
      this.path = rest;
      this.href = rest;
      this.pathname = simplePath[1];
      if (simplePath[2]) {
        this.search = simplePath[2];
        if (parseQueryString) {
          this.query = querystring.parse(this.search.substr(1));
        } else {
          this.query = this.search.substr(1);
        }
      } else if (parseQueryString) {
        this.search = '';
        this.query = {};
      }
      return this;
    }
  }

  var proto = protocolPattern.exec(rest);
  if (proto) {
    proto = proto[0];
    var lowerProto = proto.toLowerCase();
    this.protocol = lowerProto;
    rest = rest.substr(proto.length);
  }

  // figure out if it's got a host
  // user@server is *always* interpreted as a hostname, and url
  // resolution will treat //foo/bar as host=foo,path=bar because that's
  // how the browser resolves relative URLs.
  if (slashesDenoteHost || proto || rest.match(/^\/\/[^@\/]+@[^@\/]+/)) {
    var slashes = rest.substr(0, 2) === '//';
    if (slashes && !(proto && hostlessProtocol[proto])) {
      rest = rest.substr(2);
      this.slashes = true;
    }
  }

  if (!hostlessProtocol[proto] &&
      (slashes || (proto && !slashedProtocol[proto]))) {

    // there's a hostname.
    // the first instance of /, ?, ;, or # ends the host.
    //
    // If there is an @ in the hostname, then non-host chars *are* allowed
    // to the left of the last @ sign, unless some host-ending character
    // comes *before* the @-sign.
    // URLs are obnoxious.
    //
    // ex:
    // http://a@b@c/ => user:a@b host:c
    // http://a@b?@c => user:a host:c path:/?@c

    // v0.12 TODO(isaacs): This is not quite how Chrome does things.
    // Review our test case against browsers more comprehensively.

    // find the first instance of any hostEndingChars
    var hostEnd = -1;
    for (var i = 0; i < hostEndingChars.length; i++) {
      var hec = rest.indexOf(hostEndingChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd))
        hostEnd = hec;
    }

    // at this point, either we have an explicit point where the
    // auth portion cannot go past, or the last @ char is the decider.
    var auth, atSign;
    if (hostEnd === -1) {
      // atSign can be anywhere.
      atSign = rest.lastIndexOf('@');
    } else {
      // atSign must be in auth portion.
      // http://a@b/c@d => host:b auth:a path:/c@d
      atSign = rest.lastIndexOf('@', hostEnd);
    }

    // Now we have a portion which is definitely the auth.
    // Pull that off.
    if (atSign !== -1) {
      auth = rest.slice(0, atSign);
      rest = rest.slice(atSign + 1);
      this.auth = decodeURIComponent(auth);
    }

    // the host is the remaining to the left of the first non-host char
    hostEnd = -1;
    for (var i = 0; i < nonHostChars.length; i++) {
      var hec = rest.indexOf(nonHostChars[i]);
      if (hec !== -1 && (hostEnd === -1 || hec < hostEnd))
        hostEnd = hec;
    }
    // if we still have not hit it, then the entire thing is a host.
    if (hostEnd === -1)
      hostEnd = rest.length;

    this.host = rest.slice(0, hostEnd);
    rest = rest.slice(hostEnd);

    // pull out port.
    this.parseHost();

    // we've indicated that there is a hostname,
    // so even if it's empty, it has to be present.
    this.hostname = this.hostname || '';

    // if hostname begins with [ and ends with ]
    // assume that it's an IPv6 address.
    var ipv6Hostname = this.hostname[0] === '[' &&
        this.hostname[this.hostname.length - 1] === ']';

    // validate a little.
    if (!ipv6Hostname) {
      var hostparts = this.hostname.split(/\./);
      for (var i = 0, l = hostparts.length; i < l; i++) {
        var part = hostparts[i];
        if (!part) continue;
        if (!part.match(hostnamePartPattern)) {
          var newpart = '';
          for (var j = 0, k = part.length; j < k; j++) {
            if (part.charCodeAt(j) > 127) {
              // we replace non-ASCII char with a temporary placeholder
              // we need this to make sure size of hostname is not
              // broken by replacing non-ASCII by nothing
              newpart += 'x';
            } else {
              newpart += part[j];
            }
          }
          // we test again with ASCII char only
          if (!newpart.match(hostnamePartPattern)) {
            var validParts = hostparts.slice(0, i);
            var notHost = hostparts.slice(i + 1);
            var bit = part.match(hostnamePartStart);
            if (bit) {
              validParts.push(bit[1]);
              notHost.unshift(bit[2]);
            }
            if (notHost.length) {
              rest = '/' + notHost.join('.') + rest;
            }
            this.hostname = validParts.join('.');
            break;
          }
        }
      }
    }

    if (this.hostname.length > hostnameMaxLen) {
      this.hostname = '';
    } else {
      // hostnames are always lower case.
      this.hostname = this.hostname.toLowerCase();
    }

    if (!ipv6Hostname) {
      // IDNA Support: Returns a punycoded representation of "domain".
      // It only converts parts of the domain name that
      // have non-ASCII characters, i.e. it doesn't matter if
      // you call it with a domain that already is ASCII-only.
      this.hostname = punycode.toASCII(this.hostname);
    }

    var p = this.port ? ':' + this.port : '';
    var h = this.hostname || '';
    this.host = h + p;
    this.href += this.host;

    // strip [ and ] from the hostname
    // the host field still retains them, though
    if (ipv6Hostname) {
      this.hostname = this.hostname.substr(1, this.hostname.length - 2);
      if (rest[0] !== '/') {
        rest = '/' + rest;
      }
    }
  }

  // now rest is set to the post-host stuff.
  // chop off any delim chars.
  if (!unsafeProtocol[lowerProto]) {

    // First, make 100% sure that any "autoEscape" chars get
    // escaped, even if encodeURIComponent doesn't think they
    // need to be.
    for (var i = 0, l = autoEscape.length; i < l; i++) {
      var ae = autoEscape[i];
      if (rest.indexOf(ae) === -1)
        continue;
      var esc = encodeURIComponent(ae);
      if (esc === ae) {
        esc = escape(ae);
      }
      rest = rest.split(ae).join(esc);
    }
  }


  // chop off from the tail first.
  var hash = rest.indexOf('#');
  if (hash !== -1) {
    // got a fragment string.
    this.hash = rest.substr(hash);
    rest = rest.slice(0, hash);
  }
  var qm = rest.indexOf('?');
  if (qm !== -1) {
    this.search = rest.substr(qm);
    this.query = rest.substr(qm + 1);
    if (parseQueryString) {
      this.query = querystring.parse(this.query);
    }
    rest = rest.slice(0, qm);
  } else if (parseQueryString) {
    // no query string, but parseQueryString still requested
    this.search = '';
    this.query = {};
  }
  if (rest) this.pathname = rest;
  if (slashedProtocol[lowerProto] &&
      this.hostname && !this.pathname) {
    this.pathname = '/';
  }

  //to support http.request
  if (this.pathname || this.search) {
    var p = this.pathname || '';
    var s = this.search || '';
    this.path = p + s;
  }

  // finally, reconstruct the href based on what has been validated.
  this.href = this.format();
  return this;
};

// format a parsed object into a url string
function urlFormat(obj) {
  // ensure it's an object, and not a string url.
  // If it's an obj, this is a no-op.
  // this way, you can call url_format() on strings
  // to clean up potentially wonky urls.
  if (util.isString(obj)) obj = urlParse(obj);
  if (!(obj instanceof Url)) return Url.prototype.format.call(obj);
  return obj.format();
}

Url.prototype.format = function() {
  var auth = this.auth || '';
  if (auth) {
    auth = encodeURIComponent(auth);
    auth = auth.replace(/%3A/i, ':');
    auth += '@';
  }

  var protocol = this.protocol || '',
      pathname = this.pathname || '',
      hash = this.hash || '',
      host = false,
      query = '';

  if (this.host) {
    host = auth + this.host;
  } else if (this.hostname) {
    host = auth + (this.hostname.indexOf(':') === -1 ?
        this.hostname :
        '[' + this.hostname + ']');
    if (this.port) {
      host += ':' + this.port;
    }
  }

  if (this.query &&
      util.isObject(this.query) &&
      Object.keys(this.query).length) {
    query = querystring.stringify(this.query);
  }

  var search = this.search || (query && ('?' + query)) || '';

  if (protocol && protocol.substr(-1) !== ':') protocol += ':';

  // only the slashedProtocols get the //.  Not mailto:, xmpp:, etc.
  // unless they had them to begin with.
  if (this.slashes ||
      (!protocol || slashedProtocol[protocol]) && host !== false) {
    host = '//' + (host || '');
    if (pathname && pathname.charAt(0) !== '/') pathname = '/' + pathname;
  } else if (!host) {
    host = '';
  }

  if (hash && hash.charAt(0) !== '#') hash = '#' + hash;
  if (search && search.charAt(0) !== '?') search = '?' + search;

  pathname = pathname.replace(/[?#]/g, function(match) {
    return encodeURIComponent(match);
  });
  search = search.replace('#', '%23');

  return protocol + host + pathname + search + hash;
};

function urlResolve(source, relative) {
  return urlParse(source, false, true).resolve(relative);
}

Url.prototype.resolve = function(relative) {
  return this.resolveObject(urlParse(relative, false, true)).format();
};

function urlResolveObject(source, relative) {
  if (!source) return relative;
  return urlParse(source, false, true).resolveObject(relative);
}

Url.prototype.resolveObject = function(relative) {
  if (util.isString(relative)) {
    var rel = new Url();
    rel.parse(relative, false, true);
    relative = rel;
  }

  var result = new Url();
  var tkeys = Object.keys(this);
  for (var tk = 0; tk < tkeys.length; tk++) {
    var tkey = tkeys[tk];
    result[tkey] = this[tkey];
  }

  // hash is always overridden, no matter what.
  // even href="" will remove it.
  result.hash = relative.hash;

  // if the relative url is empty, then there's nothing left to do here.
  if (relative.href === '') {
    result.href = result.format();
    return result;
  }

  // hrefs like //foo/bar always cut to the protocol.
  if (relative.slashes && !relative.protocol) {
    // take everything except the protocol from relative
    var rkeys = Object.keys(relative);
    for (var rk = 0; rk < rkeys.length; rk++) {
      var rkey = rkeys[rk];
      if (rkey !== 'protocol')
        result[rkey] = relative[rkey];
    }

    //urlParse appends trailing / to urls like http://www.example.com
    if (slashedProtocol[result.protocol] &&
        result.hostname && !result.pathname) {
      result.path = result.pathname = '/';
    }

    result.href = result.format();
    return result;
  }

  if (relative.protocol && relative.protocol !== result.protocol) {
    // if it's a known url protocol, then changing
    // the protocol does weird things
    // first, if it's not file:, then we MUST have a host,
    // and if there was a path
    // to begin with, then we MUST have a path.
    // if it is file:, then the host is dropped,
    // because that's known to be hostless.
    // anything else is assumed to be absolute.
    if (!slashedProtocol[relative.protocol]) {
      var keys = Object.keys(relative);
      for (var v = 0; v < keys.length; v++) {
        var k = keys[v];
        result[k] = relative[k];
      }
      result.href = result.format();
      return result;
    }

    result.protocol = relative.protocol;
    if (!relative.host && !hostlessProtocol[relative.protocol]) {
      var relPath = (relative.pathname || '').split('/');
      while (relPath.length && !(relative.host = relPath.shift()));
      if (!relative.host) relative.host = '';
      if (!relative.hostname) relative.hostname = '';
      if (relPath[0] !== '') relPath.unshift('');
      if (relPath.length < 2) relPath.unshift('');
      result.pathname = relPath.join('/');
    } else {
      result.pathname = relative.pathname;
    }
    result.search = relative.search;
    result.query = relative.query;
    result.host = relative.host || '';
    result.auth = relative.auth;
    result.hostname = relative.hostname || relative.host;
    result.port = relative.port;
    // to support http.request
    if (result.pathname || result.search) {
      var p = result.pathname || '';
      var s = result.search || '';
      result.path = p + s;
    }
    result.slashes = result.slashes || relative.slashes;
    result.href = result.format();
    return result;
  }

  var isSourceAbs = (result.pathname && result.pathname.charAt(0) === '/'),
      isRelAbs = (
          relative.host ||
          relative.pathname && relative.pathname.charAt(0) === '/'
      ),
      mustEndAbs = (isRelAbs || isSourceAbs ||
                    (result.host && relative.pathname)),
      removeAllDots = mustEndAbs,
      srcPath = result.pathname && result.pathname.split('/') || [],
      relPath = relative.pathname && relative.pathname.split('/') || [],
      psychotic = result.protocol && !slashedProtocol[result.protocol];

  // if the url is a non-slashed url, then relative
  // links like ../.. should be able
  // to crawl up to the hostname, as well.  This is strange.
  // result.protocol has already been set by now.
  // Later on, put the first path part into the host field.
  if (psychotic) {
    result.hostname = '';
    result.port = null;
    if (result.host) {
      if (srcPath[0] === '') srcPath[0] = result.host;
      else srcPath.unshift(result.host);
    }
    result.host = '';
    if (relative.protocol) {
      relative.hostname = null;
      relative.port = null;
      if (relative.host) {
        if (relPath[0] === '') relPath[0] = relative.host;
        else relPath.unshift(relative.host);
      }
      relative.host = null;
    }
    mustEndAbs = mustEndAbs && (relPath[0] === '' || srcPath[0] === '');
  }

  if (isRelAbs) {
    // it's absolute.
    result.host = (relative.host || relative.host === '') ?
                  relative.host : result.host;
    result.hostname = (relative.hostname || relative.hostname === '') ?
                      relative.hostname : result.hostname;
    result.search = relative.search;
    result.query = relative.query;
    srcPath = relPath;
    // fall through to the dot-handling below.
  } else if (relPath.length) {
    // it's relative
    // throw away the existing file, and take the new path instead.
    if (!srcPath) srcPath = [];
    srcPath.pop();
    srcPath = srcPath.concat(relPath);
    result.search = relative.search;
    result.query = relative.query;
  } else if (!util.isNullOrUndefined(relative.search)) {
    // just pull out the search.
    // like href='?foo'.
    // Put this after the other two cases because it simplifies the booleans
    if (psychotic) {
      result.hostname = result.host = srcPath.shift();
      //occationaly the auth can get stuck only in host
      //this especially happens in cases like
      //url.resolveObject('mailto:local1@domain1', 'local2@domain2')
      var authInHost = result.host && result.host.indexOf('@') > 0 ?
                       result.host.split('@') : false;
      if (authInHost) {
        result.auth = authInHost.shift();
        result.host = result.hostname = authInHost.shift();
      }
    }
    result.search = relative.search;
    result.query = relative.query;
    //to support http.request
    if (!util.isNull(result.pathname) || !util.isNull(result.search)) {
      result.path = (result.pathname ? result.pathname : '') +
                    (result.search ? result.search : '');
    }
    result.href = result.format();
    return result;
  }

  if (!srcPath.length) {
    // no path at all.  easy.
    // we've already handled the other stuff above.
    result.pathname = null;
    //to support http.request
    if (result.search) {
      result.path = '/' + result.search;
    } else {
      result.path = null;
    }
    result.href = result.format();
    return result;
  }

  // if a url ENDs in . or .., then it must get a trailing slash.
  // however, if it ends in anything else non-slashy,
  // then it must NOT get a trailing slash.
  var last = srcPath.slice(-1)[0];
  var hasTrailingSlash = (
      (result.host || relative.host || srcPath.length > 1) &&
      (last === '.' || last === '..') || last === '');

  // strip single dots, resolve double dots to parent dir
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = srcPath.length; i >= 0; i--) {
    last = srcPath[i];
    if (last === '.') {
      srcPath.splice(i, 1);
    } else if (last === '..') {
      srcPath.splice(i, 1);
      up++;
    } else if (up) {
      srcPath.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (!mustEndAbs && !removeAllDots) {
    for (; up--; up) {
      srcPath.unshift('..');
    }
  }

  if (mustEndAbs && srcPath[0] !== '' &&
      (!srcPath[0] || srcPath[0].charAt(0) !== '/')) {
    srcPath.unshift('');
  }

  if (hasTrailingSlash && (srcPath.join('/').substr(-1) !== '/')) {
    srcPath.push('');
  }

  var isAbsolute = srcPath[0] === '' ||
      (srcPath[0] && srcPath[0].charAt(0) === '/');

  // put the host back
  if (psychotic) {
    result.hostname = result.host = isAbsolute ? '' :
                                    srcPath.length ? srcPath.shift() : '';
    //occationaly the auth can get stuck only in host
    //this especially happens in cases like
    //url.resolveObject('mailto:local1@domain1', 'local2@domain2')
    var authInHost = result.host && result.host.indexOf('@') > 0 ?
                     result.host.split('@') : false;
    if (authInHost) {
      result.auth = authInHost.shift();
      result.host = result.hostname = authInHost.shift();
    }
  }

  mustEndAbs = mustEndAbs || (result.host && srcPath.length);

  if (mustEndAbs && !isAbsolute) {
    srcPath.unshift('');
  }

  if (!srcPath.length) {
    result.pathname = null;
    result.path = null;
  } else {
    result.pathname = srcPath.join('/');
  }

  //to support request.http
  if (!util.isNull(result.pathname) || !util.isNull(result.search)) {
    result.path = (result.pathname ? result.pathname : '') +
                  (result.search ? result.search : '');
  }
  result.auth = relative.auth || result.auth;
  result.slashes = result.slashes || relative.slashes;
  result.href = result.format();
  return result;
};

Url.prototype.parseHost = function() {
  var host = this.host;
  var port = portPattern.exec(host);
  if (port) {
    port = port[0];
    if (port !== ':') {
      this.port = port.substr(1);
    }
    host = host.substr(0, host.length - port.length);
  }
  if (host) this.hostname = host;
};

},{"./util":6,"punycode":1,"querystring":4}],6:[function(require,module,exports){
'use strict';

module.exports = {
  isString: function(arg) {
    return typeof(arg) === 'string';
  },
  isObject: function(arg) {
    return typeof(arg) === 'object' && arg !== null;
  },
  isNull: function(arg) {
    return arg === null;
  },
  isNullOrUndefined: function(arg) {
    return arg == null;
  }
};

},{}],7:[function(require,module,exports){
'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
/**
 * @description A module for parsing ISO8601 durations
 */

/**
 * The pattern used for parsing ISO8601 duration (PnYnMnDTnHnMnS).
 * This does not cover the week format PnW.
 */

// PnYnMnDTnHnMnS
var numbers = '\\d+(?:[\\.,]\\d+)?';
var weekPattern = '(' + numbers + 'W)';
var datePattern = '(' + numbers + 'Y)?(' + numbers + 'M)?(' + numbers + 'D)?';
var timePattern = 'T(' + numbers + 'H)?(' + numbers + 'M)?(' + numbers + 'S)?';

var iso8601 = 'P(?:' + weekPattern + '|' + datePattern + '(?:' + timePattern + ')?)';
var objMap = ['weeks', 'years', 'months', 'days', 'hours', 'minutes', 'seconds'];

/**
 * The ISO8601 regex for matching / testing durations
 */
var pattern = exports.pattern = new RegExp(iso8601);

/** Parse PnYnMnDTnHnMnS format to object
 * @param {string} durationString - PnYnMnDTnHnMnS formatted string
 * @return {Object} - With a property for each part of the pattern
 */
var parse = exports.parse = function parse(durationString) {
  // Slice away first entry in match-array
  return durationString.match(pattern).slice(1).reduce(function (prev, next, idx) {
    prev[objMap[idx]] = parseFloat(next) || 0;
    return prev;
  }, {});
};

/**
 * Convert ISO8601 duration object to an end Date.
 *
 * @param {Object} duration - The duration object
 * @param {Date} startDate - The starting Date for calculating the duration
 * @return {Date} - The resulting end Date
 */
var end = exports.end = function end(duration, startDate) {
  // Create two equal timestamps, add duration to 'then' and return time difference
  var timestamp = startDate ? startDate.getTime() : Date.now();
  var then = new Date(timestamp);

  then.setFullYear(then.getFullYear() + duration.years);
  then.setMonth(then.getMonth() + duration.months);
  then.setDate(then.getDate() + duration.days);
  then.setHours(then.getHours() + duration.hours);
  then.setMinutes(then.getMinutes() + duration.minutes);
  // Then.setSeconds(then.getSeconds() + duration.seconds);
  then.setMilliseconds(then.getMilliseconds() + duration.seconds * 1000);
  // Special case weeks
  then.setDate(then.getDate() + duration.weeks * 7);

  return then;
};

/**
 * Convert ISO8601 duration object to seconds
 *
 * @param {Object} duration - The duration object
 * @param {Date} startDate - The starting point for calculating the duration
 * @return {Number}
 */
var toSeconds = exports.toSeconds = function toSeconds(duration, startDate) {
  var timestamp = startDate ? startDate.getTime() : Date.now();
  var now = new Date(timestamp);
  var then = end(duration, now);

  var seconds = (then.getTime() - now.getTime()) / 1000;
  return seconds;
};

exports.default = {
  end: end,
  toSeconds: toSeconds,
  pattern: pattern,
  parse: parse
};
},{}],8:[function(require,module,exports){
(function (global){
"use strict";

// ref: https://github.com/tc39/proposal-global
var getGlobal = function () {
	// the only reliable means to get the global object is
	// `Function('return this')()`
	// However, this causes CSP violations in Chrome apps.
	if (typeof self !== 'undefined') { return self; }
	if (typeof window !== 'undefined') { return window; }
	if (typeof global !== 'undefined') { return global; }
	throw new Error('unable to locate global object');
}

var global = getGlobal();

module.exports = exports = global.fetch;

// Needed for TypeScript and Webpack.
exports.default = global.fetch.bind(global);

exports.Headers = global.Headers;
exports.Request = global.Request;
exports.Response = global.Response;
}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{}],9:[function(require,module,exports){
const fetch = require('node-fetch');
const Constants = require('./util/Constants');

class Request {
    constructor(youtube) {
        this.youtube = youtube;
    }

    /**
     * Make a request to the YouTube API
     * @param {string} endpoint The endpoint to query
     * @param {object} [qs={}] Query strings
     * @returns {Promise<object>}
     */
    make(endpoint, qs = {}) {
        qs = Object.assign({ key: this.youtube.key }, qs);
        const params = Object.keys(qs).filter(k => qs[k]).map(k => `${k}=${qs[k]}`);
        return fetch(encodeURI(`https://www.googleapis.com/youtube/v3/${endpoint}${params.length ? `?${params.join('&')}` : ''}`))
            .then(result => result.json())
            .then(result => {
                if (result.error) return Promise.reject(result.error);
                return result;
            });
    }

    /**
     * Get a resource from the YouTube API
     * @param {string} type The type of resource to get
     * @param {object} [qs={}] Any other query options
     * @returns {Promise<object>}
     */
    getResource(type, qs = {}) {
        qs = Object.assign({ part: Constants.PARTS[type] }, qs);
        return this.make(Constants.ENDPOINTS[type], qs).then(result =>
            result.items.length ? result.items[0] : Promise.reject(new Error(`resource ${result.kind} not found`))
        );
    }

    /**
     * Get a resource from the YouTube API, by ID
     * @param {string} type The type of resource to get
     * @param {string} id The ID of the resource to get
     * @param {object} [qs={}] Any other query options
     * @returns {Promise<object>}
     */
    getResourceByID(type, id, qs = {}) {
        return this.getResource(type, Object.assign(qs, { id }));
    }

    /**
     * Get a video from the YouTube API
     * @param {string} id The video to get
     * @param {object} [options] Any request options
     * @returns {Promise<object>}
     */
    getVideo(id, options) {
        return this.getResourceByID('Videos', id, options);
    }

    /**
     * Get a playlist from the YouTube API
     * @param {string} id The playlist to get
     * @param {object} [options] Any request options
     * @returns {Promise<object>}
     */
    getPlaylist(id, options) {
        return this.getResourceByID('Playlists', id, options);
    }

    /**
     * Get a channel from the YouTube API
     * @param {string} id The channel to get
     * @param {object} [options] Any request options
     * @returns {Promise<object>}
     */
    getChannel(id, options) {
        return this.getResourceByID('Channels', id, options);
    }

    /**
     * Fetch a paginated resource.
     * @param {string} endpoint The endpoint to query.
     * @param {number} [count=Infinity] How many results to retrieve.
     * @param {Object} [options={}] Additional options to send.
     * @param {Array} [fetched=[]] Previously fetched resources.
     * @param {?string} [pageToken] The page token to retrieve.
     * @returns {Promise<Array<object>>}
     */
    getPaginated(endpoint, count = Infinity, options = {}, fetched = [], pageToken = null) {
        if(count < 1) return Promise.reject('Cannot fetch less than 1.');

        const limit = count > 50 ? 50 : count;
        return this.make(endpoint, Object.assign(options, { pageToken, maxResults: limit })).then(result => {
            const results = fetched.concat(result.items);
            if(result.nextPageToken && limit !== count) return this.getPaginated(endpoint, count - limit, options, results, result.nextPageToken);
            return results;
        });
    }
}

module.exports = Request;

},{"./util/Constants":14,"node-fetch":8}],10:[function(require,module,exports){
const Request = require('./Request');

const Video = require('./structures/Video');
const Playlist = require('./structures/Playlist');
const Channel = require('./structures/Channel');

const util = require('./util');
const Constants = require('./util/Constants');

/**
 * Information about a thumbnail
 * @typedef {Object} Thumbnail
 * @property {string} url The URL of this thumbnail
 * @property {number} width The width of this thumbnail
 * @property {number} height The height of this thumbnail
 */

/**
 * The YouTube API module
 */
class YouTube {
    /**
     * @param {string} key The YouTube Data API v3 key to use
     */
    constructor(key) {
        if (typeof key !== 'string') throw new Error('The YouTube API key you provided was not a string.');
        /**
         * The YouTube Data API v3 key
         * @type {?string}
         */
        this.key = key;
        Object.defineProperty(this, 'key', { enumerable: false });

        this.request = new Request(this);
    }

    /**
     * Make a request to the YouTube API
     * @param {string} endpoint The endpoint of the API
     * @param {Object} qs The query string options
     * @returns {Promise<Object>}
     */

    /**
     * Get a video by URL or ID
     * @param {string} url The video URL or ID
     * @param {Object} [options = {}] Options to request with the video.
     * @returns {Promise<?Video>}
     * @example
     * API.getVideo('https://www.youtube.com/watch?v=dQw4w9WgXcQ')
     *  .then(results => {
     *    console.log(`The video's title is ${results[0].title}`);
     *  })
     *  .catch(console.error);
     */
    getVideo(url, options = {}) {
        const id = Video.extractID(url);
        if (!id) return Promise.reject(new Error(`No video ID found in URL: ${url}`));
        return this.getVideoByID(id, options);
    }

    /**
     * Get a video by ID
     * @param {string} id The video ID
     * @param {Object} [options = {}] Options to request with the video.
     * @returns {Promise<?Video>}
     * @example
     * API.getVideoByID('3odIdmuFfEY')
     *  .then(results => {
     *    console.log(`The video's title is ${results[0].title}`);
     *  })
     *  .catch(console.error);
     */
    getVideoByID(id, options = {}) {
        return this.request.getVideo(id, options).then(result => result ? new Video(this, result) : null);
    }

    /**
     * Get a playlist by URL or ID
     * @param {string} url The playlist URL or ID
     * @param {Object} [options = {}] Options to request with the playlist.
     * @returns {Promise<?Playlist>}
     * @example
     * API.getPlaylist('https://www.youtube.com/playlist?list=PLuY9odN8x9puRuCxiddyRzJ3F5jR-Gun9')
     *  .then(results => {
     *    console.log(`The playlist's title is ${results[0].title}`);
     *  })
     *  .catch(console.error);
     */
    getPlaylist(url, options = {}) {
        const id = Playlist.extractID(url);
        if (!id) return Promise.reject(new Error(`No playlist ID found in URL: ${url}`));
        return this.getPlaylistByID(id, options);
    }

    /**
     * Get a playlist by ID
     * @param {string} id The playlist ID
     * @param {Object} [options = {}] Options to request with the playlist.
     * @returns {Promise<?Playlist>}
     * @example
     * API.getPlaylistByID('PL2BN1Zd8U_MsyMeK8r9Vdv1lnQGtoJaSa')
     *  .then(results => {
     *    console.log(`The playlist's title is ${results[0].title}`);
     *  })
     *  .catch(console.error);
     */
    getPlaylistByID(id, options = {}) {
        return this.request.getPlaylist(id, options).then(result => result ? new Playlist(this, result) : null);
    }

    /**
     * Get a channel by URL or ID
     * @param {string} url The channel URL or ID
     * @param {Object} [options = {}] Options to request with the channel.
     * @returns {Promise<?Channel>}
     * @example
     * API.getChannel('https://www.youtube.com/channel/UC477Kvszl9JivqOxN1dFgPQ')
     *  .then(results => {
     *    console.log(`The channel's title is ${results[0].title}`);
     *  })
     *  .catch(console.error);
     */
    getChannel(url, options = {}) {
        const id = Channel.extractID(url);
        if (!id) return Promise.reject(new Error(`No channel ID found in URL: ${url}`));
        return this.getChannelByID(id, options);
    }

    /**
     * Get a channel by ID
     * @param {string} id The channel ID
     * @param {Object} [options = {}] Options to request with the channel.
     * @returns {Promise<?Channel>}
     * @example
     * API.getChannelByID('UC477Kvszl9JivqOxN1dFgPQ')
     *  .then(results => {
     *    console.log(`The channel's title is ${results[0].title}`);
     *  })
     *  .catch(console.error);
     */
    getChannelByID(id, options = {}) {
        return this.request.getChannel(id, options).then(result => result ? new Channel(this, result) : null);
    }

    /**
     * Search YouTube for videos, playlists, and channels
     * @param {string} query The string to search for
     * @param {number} [limit = 5] Maximum results to obtain
     * @param {Object} [options] Additional options to pass to the API request
     * @returns {Promise<Array<Video|Playlist|Channel|null>>}
     * @example
     * API.search('Centuries')
     *  .then(results => {
     *    console.log(`I got ${results.length} results`);
     *  })
     *  .catch(console.error);
     */
    search(query, limit = 5, options = {}) {
        return this.request.getPaginated(Constants.ENDPOINTS.Search, limit, Object.assign(options, { q: query, part: Constants.PARTS.Search }))
            .then(result => result.map(item => {
                if (item.id.kind === Constants.KINDS.Video) return new Video(this, item);
                if (item.id.kind === Constants.KINDS.Playlist) return new Playlist(this, item);
                if (item.id.kind === Constants.KINDS.Channel) return new Channel(this, item);
                return null;
            }));
    }

    /**
     * Search YouTube for videos
     * @param {string} query The string to search for
     * @param {number} [limit = 5] Maximum results to obtain
     * @param {Object} [options] Additional options to pass to the API request
     * @returns {Promise<Video[]>}
     * @example
     * API.searchVideos('Centuries')
     *  .then(results => {
     *    console.log(`I got ${results.length} videos`);
     *  })
     *  .catch(console.error);
     */
    searchVideos(query, limit = 5, options = {}) {
        return this.search(query, limit, Object.assign(options, { type: 'video' }));
    }

    /**
     * Search YouTube for playlists
     * @param {string} query The string to search for
     * @param {number} [limit = 5] Maximum results to obtain
     * @param {Object} [options] Additional options to pass to the API request
     * @returns {Promise<Playlist[]>}
     * @example
     * API.searchPlaylists('Centuries')
     *  .then(results => {
     *    console.log(`I got ${results.length} playlists`);
     *  })
     *  .catch(console.error);
     */
    searchPlaylists(query, limit = 5, options = {}) {
        return this.search(query, limit, Object.assign(options, { type: 'playlist' }));
    }

    /**
     * Search YouTube for channels
     * @param {string} query The string to search for
     * @param {number} [limit = 5] Maximum results to obtain
     * @param {Object} [options] Additional options to pass to the API request
     * @returns {Promise<Channel[]>}
     * @example
     * API.searchChannels('Centuries')
     *  .then(results => {
     *    console.log(`I got ${results.length} channels`);
     *  })
     *  .catch(console.error);
     */
    searchChannels(query, limit = 5, options = {}) {
        return this.search(query, limit, Object.assign(options, { type: 'channel' }));
    }
}

YouTube.Video = Video;
YouTube.Playlist = Playlist;
YouTube.Channel = Channel;
YouTube.util = util;

module.exports = YouTube;

},{"./Request":9,"./structures/Channel":11,"./structures/Playlist":12,"./structures/Video":13,"./util":15,"./util/Constants":14}],11:[function(require,module,exports){
const { parseURL } = require('../util');
const Constants = require('../util/Constants');

/**
 * Represents a YouTube channel
 * @class
 */
class Channel {
    /**
     * @param {YouTube} youtube The YouTube instance creating this
     * @param {Object} data The data of the channel
     */
    constructor(youtube, data) {
        /**
         * The YouTube instance that created this
         * @type {YouTube}
         */
        this.youtube = youtube;
        Object.defineProperty(this, 'youtube', { enumerable: false });

        /**
         * The type to filter search results
         * @type {string}
         */
        this.type = 'channel';

        this._patch(data);
    }

    _patch(data) {
        if (!data) return;

        /**
         * Raw data from the YouTube API
         * @type {object}
         */
        this.raw = data;

        /**
         * Whether this is a full channel object.
         * @type {boolean}
         */
        this.full = data.kind === Constants.KINDS.Channel;

        /**
         * The YouTube resource from which this channel was created.
         * @type {string}
         */
        this.kind = data.kind;

        /**
         * This channel's ID
         * @type {string}
         * @name Channel#id
         */

        /**
         * This channel's title
         * @type {?string}
         * @name Channel#title
         */

        switch (data.kind) {
            case Constants.KINDS.Playlist:
            case Constants.KINDS.PlaylistItem:
            case Constants.KINDS.Video:
                if (data.snippet) {
                    this.id = data.snippet.channelId;
                    this.title = data.snippet.channelTitle;
                    break;
                } else {
                    throw new Error('Attempted to make a channel out of a resource with no channel data.');
                }
            case Constants.KINDS.SearchResult:
                if (data.id.kind === Constants.KINDS.Channel) {
                    this.id = data.id.channelId;
                    break;
                } else if (data.snippet) {
                    this.id = data.snippet.channelId;
                    this.title = data.snippet.channelTitle;
                    break;
                } else {
                    throw new Error('Attempted to make a channel out of a search result with no channel data.');
                }
            case Constants.KINDS.Channel:
                this.id = data.id;
                if (data.snippet) {
                    this.title = data.snippet.title;

                    /**
                     * This channel's description
                     * @type {?string}
                     * @name Channel#description
                     */
                    this.description = data.snippet.description;

                    /**
                     * The channel's custom URL if it has one
                     * @type {?string}
                     */
                    this.customURL = data.snippet.customUrl;

                    /**
                     * The channel's creation date
                     * @type {?Date}
                     * @name Channel#publishedAt
                     */
                    this.publishedAt = new Date(data.snippet.publishedAt);

                    /**
                     * The channel's thumbnails: available types are 'default', 'medium', and 'high'
                     * @type {?Object.<string, Thumbnail>}
                     */
                    this.thumbnails = data.snippet.thumbnails;

                    /**
                     * The channel's default language
                     * @type {?string}
                     */
                    this.defaultLanguage = data.snippet.defaultLanguage;

                    /**
                     * Information about the channel as specified in the `hl` query parameter
                     * @type {?{title: string, description: string}}
                     */
                    this.localized = data.snippet.localized;

                    /**
                     * The country of the channel
                     * @type {?string}
                     */
                    this.country = data.snippet.country;
                }

                if (data.contentDetails) {
                    /**
                     * Playlists associated with this channel; all values are playlist IDs
                     * @type {?Object}
                     * @property {?string} likes The channel's liked videos
                     * @property {?string} favorites The channel's favorited videos (note: favorited videos are deprecated)
                     * @property {?string} uploads The channel's uploaded videos
                     */
                    this.relatedPlaylists = data.contentDetails.relatedPlaylists;
                }

                if (data.statistics) {
                    /**
                     * The number of times the channel has been viewed
                     * @type {?number}
                     */
                    this.viewCount = data.statistics.viewCount;

                    /**
                     * The number of comments on the channel
                     * @type {?number}
                     */
                    this.commentCount = data.statistics.commentCount;

                    /**
                     * The number of subscribers the channel has
                     * @type {?number}
                     */
                    this.subscriberCount = data.statistics.subscriberCount;

                    /**
                     * Whether the channel's subscriber count is public
                     * @type {?boolean}
                     */
                    this.hiddenSubscriberCount = data.statistics.hiddenSubscriberCount;

                    /**
                     * The number of videos this channel has uploaded
                     * @type {?number}
                     */
                    this.videoCount = data.statistics.videoCount;
                }
                break;
            default:
                throw new Error(`Unknown channel kind: ${data.kind}.`);
        }

        return this;
    }

    /**
     * Fetch the full representation of this channel.
     * @param {object} [options] Any extra query params
     * @returns {Channel}
     */
    fetch(options) {
        return this.youtube.request.getChannel(this.id, options).then(this._patch.bind(this));
    }

    /**
     * The URL to this channel
     * @type {string}
     */
    get url() {
        return `https://www.youtube.com/channel/${this.id}`;
    }

    /**
     * Get a channel ID from a string (URL or ID)
     * @param {string} url The string to get the ID from
     * @returns {?string}
     */
    static extractID(url) {
        return parseURL(url).channel;
    }
}

module.exports = Channel;

},{"../util":15,"../util/Constants":14}],12:[function(require,module,exports){
const { parseURL } = require('../util');
const Constants = require('../util/Constants');
const Video = require('./Video');
const Channel = require('./Channel');

/** Represents a YouTube playlist */
class Playlist {
    /**
     * @param {YouTube} youtube The YouTube instance creating this
     * @param {Object} data The data of the playlist
     */
    constructor(youtube, data) {
        /**
         * The YouTube instance that created this
         * @type {YouTube}
         */
        this.youtube = youtube;
        Object.defineProperty(this, 'youtube', { enumerable: false });

        /**
         * The type to filter search results
         * @type {string}
         */
        this.type = 'playlist';

        /**
         * Videos in this playlist.  Available after calling {@link Playlist#getVideos}.
         * @type {Array<Video>}
         */
        this.videos = [];

        this._patch(data);
    }

    _patch(data) {
        if (!data) return;

        this.raw = data;

        /**
         * The channel this playlist is in
         * @type {Channel}
         */
        this.channel = new Channel(this.youtube, data);

        /**
         * This playlist's ID
         * @type {string}
         * @name Playlist#id
         */

        switch (data.kind) {
            case Constants.KINDS.SearchResult:
                if (data.id.kind === Constants.KINDS.Playlist) this.id = data.id.playlistId;
                else throw new Error('Attempted to make a playlist out of a non-playlist search result.');
                break;
            case Constants.KINDS.Playlist:
                this.id = data.id;
                break;
            case Constants.KINDS.PlaylistItem:
                if (data.snippet) this.id = data.snippet.playlistId;
                else throw new Error('Attempted to make a playlist out of a resource with no playlist data.');
                return this; // don't pull extra info from playlist item info
            default:
                throw new Error(`Unknown playlist kind: ${data.kind}.`);
        }

        if (data.snippet) {
            /**
             * This playlist's title
             * @type {?string}
             */
            this.title = data.snippet.title;

            /**
             * This playlist's description
             * @type {?string}
             */
            this.description = data.snippet.description;

            /**
             * The date/time this playlist was published
             * @type {?Date}
             */
            this.publishedAt = new Date(data.snippet.publishedAt);

            /**
             * Thumbnails for this playlist
             * @type {?Object.<string, Thumbnail>}
             */
            this.thumbnails = data.snippet.thumbnails;

            /**
             * Channel title of this playlist
             * @type {?string}
             */
            this.channelTitle = data.snippet.channelTitle;

            /**
             * The language in this playlist's title and description
             * @type {?string}
             */
            this.defaultLanguage = data.snippet.defaultLanguage;

            /**
             * Information about the playlist as specified in the `hl` parameter
             * @type {?{title: string, description: string}}
             */
            this.localized = data.snippet.localized;
        }

        if (data.status) {
            /**
             * The privacy status of this video
             * @type {string}
             */
            this.privacy = data.status.privacyStatus;
        }

        if (data.contentDetails) {
            /**
             * The total number of videos in this playlist
             * @type {number}
             */
            this.length = data.contentDetails.itemCount;
        }

        if (data.player) {
            /**
             * A string with an iframe tag for embedding this playlist
             * @type {string}
             */
            this.embedHTML = data.player.embedHtml;
        }

        return this;
    }

    /**
     * The URL to this playlist
     * @type {string}
     */
    get url() {
        return `https://www.youtube.com/playlist?list=${this.id}`;
    }

    /**
     * Fetch the full representation of this playlist.
     * @param {object} [options] Any extra query params
     * @returns {Playlist}
     */
    fetch(options) {
        return this.youtube.request.getPlaylist(this.id, options).then(this._patch.bind(this));
    }

    /**
     * Gets videos in the playlist
     * @param {Number} [limit] Maximum number of videos to obtain.  Fetches all if not provided.
     * @param {Object} [options] Options to retrieve for each video.
     * @returns {Promise<Video[]>}
     */
    getVideos(limit, options) {
        return this.youtube.request.getPaginated(
            Constants.ENDPOINTS.PlaylistItems,
            limit,
            Object.assign({ playlistId: this.id, part: Constants.PARTS.PlaylistItems }, options)
        ).then(items => this.videos = items.map(i => new Video(this.youtube, i)));
    }

    /**
     * Get a playlist ID from a string (URL or ID)
     * @param {string} url The string to get the ID from
     * @returns {?string}
     */
    static extractID(url) {
        return parseURL(url).playlist;
    }
}

module.exports = Playlist;

},{"../util":15,"../util/Constants":14,"./Channel":11,"./Video":13}],13:[function(require,module,exports){
const duration = require('iso8601-duration');
const { parseURL } = require('../util');
const Constants = require('../util/Constants');
const Channel = require('./Channel');

/** Represents a YouTube video */
class Video {
    /**
     * @param {YouTube} youtube The YouTube instance creating this
     * @param {Object} data The data of the video
     */
    constructor(youtube, data) {
        /**
         * The YouTube instance that created this
         * @type {YouTube}
         */
        this.youtube = youtube;
        Object.defineProperty(this, 'youtube', { enumerable: false });

        /**
         * The type to filter search results
         * @type {string}
         */
        this.type = 'video';

        this._patch(data);
    }

    _patch(data) {
        if (!data) return;

        /**
         * The raw data from the YouTube API.
         * @type {object}
         */
        this.raw = data;

        /**
         * Whether this is a full (returned from the videos API end point) or partial video (returned
         * as part of another resource).
         * @type {boolean}
         */
        this.full = data.kind === Constants.KINDS.Video;

        /**
         * The resource that this video was created from.
         * @type {string}
         */
        this.kind = data.kind;

        /**
         * This video's ID
         * @type {string}
         * @name Video#id
         */

        switch (data.kind) {
            case Constants.KINDS.PlaylistItem:
                if (data.snippet) {
                    if (data.snippet.resourceId.kind === Constants.KINDS.Video) this.id = data.snippet.resourceId.videoId;
                    else throw new Error('Attempted to make a video out of a non-video playlist item.');
                    break;
                } else {
                    throw new Error('Attempted to make a video out of a playlist item with no video data.');
                }
            case Constants.KINDS.Video:
                this.id = data.id;
                break;
            case Constants.KINDS.SearchResult:
                if (data.id.kind === Constants.KINDS.Video) this.id = data.id.videoId;
                else throw new Error('Attempted to make a video out of a non-video search result.');
                break;
            default:
                throw new Error(`Unknown video kind: ${data.kind}.`);
        }

        if (data.snippet) {
            /**
             * This video's title
             * @type {string}
             */
            this.title = data.snippet.title;

            /**
             * This video's description
             * @type {string}
             */
            this.description = data.snippet.description;

            /**
             * The thumbnails of this video.
             * @type {Object.<'default', 'medium', 'high', 'standard', 'maxres'>}
             */
            this.thumbnails = data.snippet.thumbnails;

            /**
             * The date/time this video was published
             * @type {Date}
             */
            this.publishedAt = new Date(data.snippet.publishedAt);

            /**
             * The channel this video is in.
             * @type {Channel}
             */
            this.channel = new Channel(this.youtube, data);
        }

        if(data.contentDetails) {
            /**
             * An object containing time period information. All properties are integers, and do not include the lower
             * precision ones.
             * @typedef {Object} DurationObject
             * @property {number} [hours] How many hours the video is long
             * @property {number} [minutes] How many minutes the video is long
             * @property {number} [seconds] How many seconds the video is long
             */

            /**
             * The duration of the video
             * @type {?DurationObject}
             */
            this.duration = data.contentDetails.duration ? duration.parse(data.contentDetails.duration) : null;
        }

        return this;
    }

    /**
     * The maxiumum available resolution thumbnail.
     * @type {object}
     */
    get maxRes() {
        const t = this.thumbnails;
        return t.maxres || t.standard || t.high || t.medium || t.default;
    }

    /**
     * The URL to this video
     * @type {string}
     */
    get url() {
        return `https://www.youtube.com/watch?v=${this.id}`;
    }

    /**
     * The short URL to this video
     * @type {string}
     */
    get shortURL() {
        return `https://youtu.be/${this.id}`;
    }

    /**
     * The duration of the video in seconds
     * @type {number}
     */
    get durationSeconds() {
        return this.duration ? duration.toSeconds(this.duration) : -1;
    }

    /**
     * Fetch the full representation of this video.
     * @param {object} [options] Any extra query params
     * @returns {Video}
     */
    fetch(options) {
        return this.youtube.request.getVideo(this.id, options).then(this._patch.bind(this));
    }

    /**
     * Get a video ID from a string (URL or ID)
     * @param {string} url The string to get the ID from
     * @returns {?string}
     */
    static extractID(url) {
        return parseURL(url).video;
    }
}

module.exports = Video;

},{"../util":15,"../util/Constants":14,"./Channel":11,"iso8601-duration":7}],14:[function(require,module,exports){
exports.PARTS = {
    Search: 'snippet',
    Videos: 'snippet,contentDetails',
    Playlists: 'snippet',
    PlaylistItems: 'snippet,status',
    Channels: 'snippet'
};

exports.KINDS = {
    Video: 'youtube#video',
    PlaylistItem: 'youtube#playlistItem',
    Playlist: 'youtube#playlist',
    SearchResult: 'youtube#searchResult',
    Channel: 'youtube#channel'
};

exports.ENDPOINTS = {
    PlaylistItems: 'playlistItems',
    Channels: 'channels',
    Videos: 'videos',
    Playlists: 'playlists',
    Search: 'search'
};

},{}],15:[function(require,module,exports){
const { parse } = require('url');

/**
 * Parse a string as a potential YouTube resource URL.
 * @param {string} url
 * @returns {{video: ?string, channel: ?string, playlist: ?string}}
 */
exports.parseURL = (url) => {
    const parsed = parse(url, true);
    switch (parsed.hostname) {
        case 'www.youtube.com':
        case 'youtube.com':
        case 'm.youtube.com': {
            const idRegex = /^[a-zA-Z0-9-_]+$/;
            if (parsed.pathname === '/watch') {
                if (!idRegex.test(parsed.query.v)) return {};
                const response = { video: parsed.query.v };
                if (parsed.query.list) response.playlist = parsed.query.list;
                return response;
            } else if (parsed.pathname === '/playlist') {
                if(!idRegex.test(parsed.query.list)) return {};
                return { playlist: parsed.query.list };
            } else if (parsed.pathname.startsWith('/channel/')) {
                const id = parsed.pathname.replace('/channel/', '');
                if (!idRegex.test(id)) return {};
                return { channel: id };
            }

            return {};
        }
        case 'youtu.be':
            return { video: /^\/[a-zA-Z0-9-_]+$/.test(parsed.pathname) ? parsed.pathname.slice(1) : null };
        default:
            return {};
    }
};

},{"url":5}],16:[function(require,module,exports){
const YouTube = require('simple-youtube-api');
const ytb = new YouTube(key='AIzaSyAmm0tAApu3XivE_1n7xbqSkZxs2Ql59HU');

const e = "sarah";
const commands= [
  `joue`,
  `ajoute à la playlist`,
  `stop`,
  `pause`,
  `suivant`,
  `monte le son`,
  `baisse le son`,
  `mets le son à`,
  `continue`,
  `arrête le PC`,
  `aide-moi`
]
const alias = [
  `ajoute à la file d'attente`,
  `ajoute`,
  `pose`,
  `quelles sont les commandes`,
  `arrête tout`,
  `arrête la musique`,
  `baisse le volume`,
  `baisse le volume`,
  `mets le volume à`
]


// ########################### CLASS ET FONCTION #############################

function play(args) {
  ytb.searchVideos(args, 1)
      .then(results => {

          console.log("Url : "+results[0].url);

          let id = (results[0].url).replace("https://www.youtube.com/watch?v=", '');
          player.loadVideoById(id);
          /*
          let el = document.getElementById('player');
          el.src=`https://www.youtube.com/embed/${id}?disablekb=1&fs=0&iv_load_policy=3&modestbranding=1&rel=0&enablejsapi=1&origin=http%3A%2F%2F${e.toLowerCase()}.localhost&widgetid=1&autoplay=1`;
          */
        })
      .catch(console.log);
}

function forward() {
  if (!ytb_queue[0]) {
    if(player.getPlayerState() == 1) {
      player.stopVideo();
    }
    Chat.out("La playlist est finie");
    return
  } else {
    Chat.type(`Musique suivante (${ytb_queue[0]})`);
    ytb.searchVideos(ytb_queue[0], 1)
        .then(results => {
            ytb_queue.shift();
            console.log("Url : "+results[0].url);

            let id = (results[0].url).replace("https://www.youtube.com/watch?v=", '');
            player.loadVideoById(id);
          })
          .catch(console.log);
    }
}



class chat {
  constructor(el) {
  }
  //Message recu :
  in(message) {


    //Mappage du message:
    //Master world (e):
    var masterworld = false;
    if(message.includes(e)) {
      masterworld= true;
    }
    console.log(`masterworld: ${masterworld}`);
    //Commande
    var commande = message.replace(e, '');
    for (let i = 0; i < commands.length; i++) {
      if(commande.includes(commands[i])) {
        commande =commands[i];
        console.log(`Cmd: ${commande}`);
      }
    }
    //alias
    for (let i = 0; i < alias.length; i++) {
      if(commande.includes(alias[i])) {
        commande =alias[i];
        console.log(`Cmd: ${commande}`);
      }
    }
    //Arguments
    var args = (message.replace(e,'')).replace(commande, '');
    console.log(`Args: ${args}`);



    let el = document.getElementById('console');
    el.innerHTML+=`<div class="line in">${message}</div>`;



    // Action en fonction du message :
    this.act(message, commande, args);
  }

  act(message, commande, args) {
    var actualVolume = player.getVolume();
    switch(commande) {
    case "aide-moi":
    case "quelles sont les commandes":
      this.out("Voici toutes les commandes :");
      this.type(commands);
      break;
    case "joue":
      this.out(`D'accord, je joue ${args}`);
      play(args);
      break;
    case "ajoute à la playlist":
    case "ajoute à la file d'attente":
    case "ajoute":
      this.out(`J'ai ajouté ${args} à la playlist`);
      ytb_queue.push(args);
      break;
    case "stop":
    case "arrête tout":
    case "arrête la musique":
      player.stopVideo();
      this.out("J'arrête la musique");
      break;
    case "pause":
    case "pose":
      this.type("Vidéo en pause");
      player.pauseVideo();
      break;
    case "continue":
      this.type("C'est repartit!");
      player.playVideo();
      break;
    case "suivant":
      forward();
      break;
    case "baisse le son":
    case "baisse le volume":
      if (actualVolume==0) {
        this.out('Le son est déjà au minimum');
      } else {
        this.type(`Le son est désormais à ${actualVolume}`);
        player.setVolume(actualVolume - 10);
      }
      break;
    case "monte le son":
    case "monte le volume":
      if (actualVolume==100) {
        this.out('Le son est déjà au maximum');
      } else {
        this.type(`Le son est désormais à ${actualVolume}`);
        player.setVolume(actualVolume + 10);
      }
      break;
    case "mets le son à":
    case "mets le volume à":
      if (args > 100) {
        this.type('Volume mis à 100');
        player.setVolume(100);
      } else if (args < 0) {
        this.type('Volume mis à 0');
        player.setVolume(0);
      } else if (args == "zéro") {
        player.setVolume(0);
        this.type(`C'est fait!`);
      } else {
        player.setVolume(args);
        this.type(`C'est fait!`);
      }
      break;
    case "arrête le PC":
      document.location.href ="/shutdown.php";
      break;
    default:
      this.type("Pas compris");
      break;
    // code block
    demarrerReconnaissanceVocale();
  }
}


  out(message) {
    // Synthèse vocale de ce qui a été reconnu
    var u = new SpeechSynthesisUtterance();
    u.text = message;
    u.lang = 'fr-FR';
    u.rate = 1.2;
    speechSynthesis.speak(u);
    let el = document.getElementById('console');
    el.innerHTML+=`<div class="line out">${message}</div>`;
  }
  type(message) {
    let el = document.getElementById('console');
    el.innerHTML+=`<div class="line out">${message}</div>`;
  }
  say(message) {
    // Synthèse vocale de ce qui a été reconnu
    var u = new SpeechSynthesisUtterance();
    u.text = message;
    u.lang = 'fr-FR';
    u.rate = 1.2;
    speechSynthesis.speak(u);
  }

}

//##############################################################################
// Initialisation de la reconnaissance vocale en fonction du navigateur
// Pour l'instant, seul Google Chrome le supporte
var SpeechRecognition = SpeechRecognition ||
                          webkitSpeechRecognition ||
                          mozSpeechRecognition ||
                          msSpeechRecognition ||
                          oSpeechRecognition;

var recognition;
var lastStartedAt;
var Chat = new chat('console');

function listen() {
if (!SpeechRecognition) {
    console.log('Pas de reconnaissance vocale disponible');
    alert('Pas de reconnaissance vocale disponible');
} else {

    // Arrêt de l'ensemble des instances déjà démarrées
        if (recognition && recognition.abort) {
        recognition.abort();
        }

    // Initialisation de la reconnaissance vocale
    recognition = new SpeechRecognition();
    // Reconnaissance en continue
    recognition.continuous = true;
    // Langue française
    recognition.lang = 'fr-FR';

    // Evènement de début de la reconnaissance vocale
    recognition.onstart = function() {
        console.log('Démarrage de la reconnaissance');
    };

    // Evènement de fin de la reconnaissance vocale
    // A la fin de la reconnaissance (timeout), il est nécessaire de la redémarrer pour avoir une reconnaissance en continu
    recognition.onend = function() {
        console.log('Fin de la reconnaissance');
        var timeSinceLastStart = new Date().getTime()-lastStartedAt;
        if (timeSinceLastStart < 1000) {
            setTimeout(demarrerReconnaissanceVocale, 1000-timeSinceLastStart);
        } else {
            // Démarrage de la reconnaissance vocale
            demarrerReconnaissanceVocale();
        }
    };

    // Evènement de résultat de la reconnaissance vocale
    recognition.onresult = function (event) {
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            var texteReconnu = event.results[i][0].transcript;
            console.log("----------------------");
            console.log('Résultat = ' + texteReconnu);
            //Analyse du message
            Chat.in(texteReconnu.toLowerCase());

        }
    };

    // Démarrage de la reconnaissance vocale
    demarrerReconnaissanceVocale();
}
}


function demarrerReconnaissanceVocale() {
// Démarrage de la reconnaissance vocale
console.log("Listen..");
lastStartedAt = new Date().getTime();
    recognition.start();
}

window.onload = listen();

},{"simple-youtube-api":10}]},{},[16]);
